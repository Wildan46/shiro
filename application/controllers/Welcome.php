<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$data['pecah'] = $this->model_s->datauser();
		$this->load->view('header');
		$this->load->view('home',$data);
	}

	function tambah_data()
	{
		$this->load->view('header');
		$this->load->view('tambah_data');
	}

	function insert_data()
	{

	$email = $this->input->post('email');
	$password=  $this->input->post('password');
	$repass = $this->input->post('repass');
	$nama = $this->input->post('nama');
	$gender = $this->input->post('gender');
	$no_telp = $this->input->post('no_telp');
	$pekerjaan = $this->input->post('pekerjaan');
	$userfile = $_FILES['userfile']['name'];

	if($password == $repass)
	{
		$config['upload_path'] = ('uploads/');
		$config['allowed_types'] = 'jpg|amp|png';
		$config['file_name'] = $userfile;
		$upload = $this->upload->initialize($config);
		
		if(!$this->upload->do_upload())
		{
			$error_mes = array('error_mes'=>$this->upload->display_errors());
			$this->session->set_flashdata('error_mes',$error_mes['error_mes']);
			redirect('welcome/tambah_data');
		}

		else
		{
			$data = $this->upload->data();

			$p = array(
				'email'=>$email,
				'password'=>$password,
				'nama'=>$nama,
				'gender'=>$gender,
				'no_telp'=>$no_telp,
				'pekerjaan'=>$pekerjaan,
				'photo'=>$data['file_name']
			);

			$this->model_s->insert_data($p);
			$this->session->set_flashdata('success','data berhasil di input');
			redirect('welcome/tambah_data');
		}



	}
	else
	{
		$this->session->set_flashdata('error','password tidak cocok');
		redirect('welcome/tambah_data');
	}

	}

	function delete($id_user)
	{
		$where = array('id_user'=>$id_user);
		$this->model_s->delete($where);
		redirect('welcome');
	}

	function getdata()
	{
		$d = $this->model_s->datauser();
		$no = 1;
		foreach($d as $row)
		{
		$delete = base_url('welcome/delete').'/'.$row->id_user;
		$button = "
		<a href='$delete' onclick='return confirm(\"Yakin akan menghapus data ini ?\")' class='btn btn-danger'>
	      <span class='fa fa-trash'></span></a>
			";
		$url = base_url('uploads').'/'.$row->photo;
 		$img ="<img src='$url' width='100' height='100' class='img img-rounded'>";
		$output['data'][] = array(
			$no,
			$row->email,
			$row->password,
			$row->nama,
			$row->gender,
			$row->no_telp,
			$row->pekerjaan,
			$img,
			$button
			);
			$no++;
		}
		echo json_encode($output);

	}
}

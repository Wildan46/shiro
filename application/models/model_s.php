<?php 

class model_s extends ci_model
{
    function datauser()
    {
        $sql = $this->db->get('user_list');
        return $sql->result();
    }

    function insert_data($data)
    {
    	$sql = $this->db->insert('user_list',$data);
    	return $sql;
    }

    function delete($where)
    {
    	$sql = $this->db->delete('user_list',$where);
    	return $sql;
    }
}
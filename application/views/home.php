<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.bootstrap4.min.css">

    <a class="btn btn-primary" href="<?php echo base_url('welcome/tambah_data');?>">
        <span class="fa fa-plus"></span>
    </a>

    <div class="break1"></div>

    <table id="user" class="table table-bordered table-responsive">

        <thead>
        <th>No</th>
        <th>email</th>
        <th>password</th>
        <th>Nama</th>
        <th>Gender</th>
        <th>No Telepon</th>
        <th>Pekerjaan</th>
        <th>Photo</th>
        <th>Actions</th>
        </thead>
    
    <tbody>
     </tbody> 
   
    <tfoot>
        <th>No</th>
        <th>email</th>
        <th>password</th>
        <th>Nama</th>
        <th>Gender</th>
        <th>No Telepon</th>
        <th>Pekerjaan</th>
        <th>Photo</th>
        <th>Actions</th>
    </tfoot>
    
    </table>    

</div>

 <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
 <script src="https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap4.min.js"></script>
 <script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
<script src="<?php echo base_url('dist/jquery.min.js');?>"></script>
<script src="<?php echo base_url('dist/jquery.dataTables.min.js');?>"></script>


<script type="text/javascript">
    $(document).ready(function() {
    $('#user').DataTable({
        "processing": true,
        "ajax":"<?php echo base_url('welcome/getdata');?>",
        "order":[]
    });
} );
</script>

</body>
</html>
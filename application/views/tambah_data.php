
<?php 
$error = $this->session->flashdata('error');
$success= $this->session->flashdata('success');
$error_mes = $this->session->flashdata('error_mes');
if(isset($error))
{
	?>
	<div class="alert alert-danger">
	<div class="text-center">
		<strong><?php echo $error;?></strong>
	</div>
	</div>
	<?php
}
elseif(isset($success))
{
	?>
	<div class="alert alert-success">
		<div class="text-center">
			<strong><?php echo $success;?></strong>
			<meta http-equiv="refresh" content="2; url=<?php echo base_url('welcome');?>">
		</div>
	</div>
	<?php
}
elseif(isset($error_mes))
{
	?>
	<div class="alert alert-danger">
		<div class="text-center">
		<strong><?php echo $error_mes;?></strong>
	</div>
</div>
	<?php
}
?>




<form action="<?php echo base_url('welcome/insert_data');?>" method="post" enctype="multipart/form-data">

	<div class="form-group">
	<label>email</label>
	<input type="text" class="form-control input-md" name="email" placeholder="email">
	</div>

	<div class="form-group">
	<label>password</label>
	<input type="password" class="form-control input-md" name="password" placeholder="passowrd">
	</div>


	<div class="form-group">
	<label>ReType Password</label>
	<input type="password" class="form-control input-md" name="repass" placeholder="password">
	</div>

	<div class="form-group">
	<label>Nama</label>
	<input type="text" class="form-control input-md" name="nama" placeholder="nama">
	</div>

	<div class="form-group">
	<label>Gender</label>
	<div class="break1"></div>
	<input type="radio" name="gender" value="laki-laki">Laki-Laki
	<input type="radio" name="gender" value="perempuan">Perempuan
	</div>
	
	<div class="form-group">
	<label>No Telp</label>
	<input type="text" class="form-control input-md" name="no_telp" placeholder="No Telepon">
	</div>
	
	<label>Pekerjaan</label>
	<select name="pekerjaan" class="form-control">
		<option value="Karyawan Swasta">Karyawan Swasta</option>
		<option value="Pegawai Negeri">Pegawai Negeri</option>
		<option value="Belum Bekerja">Belum Bekerja</option>
	</select>
		
	<div class="form-group">
	<label>Image</label>
	<input type="file" name="userfile" class="btn btn-primary">	
	</div>

	<div class="modal-footer">
		<input type="submit" class="btn btn-primary">
	</div>

	</form>





</form>